package parser;


import java.io.File;
import java.util.Hashtable;

import scanner.FileScanner;
import scanner.NextTokenReturn;
import scanner.SymbolTable;
import scanner.Tokens;

/**
* Parses tokens from File Scanner
* @author Melissa Sweeney
*/

public class FileParser {
	
	private NextTokenReturn ntr = null;
	private Tokens currentToken;
	private FileScanner scanner;

	/**
	 * Gets the file name passed in from ScannerReadFile
	 * @param filename
	 */
	public FileParser(String filename) {
		// TODO Auto-generated constructor stub
		File input = new File( filename);

		scanner = new FileScanner(input, new SymbolTable());
		
		scanner.nextToken();
		currentToken = scanner.getToken();
	}
	
	/*
	 * Calls the start of the grammer program 
	 */
	public void parse(){
		program();
	}
	
	
	/**
	 * Parses the program
	 * Implements program -> program id;, declarations, subprogram_declarations, compound_statement
	 */
	public void program() {
		System.out.println("In Program");
		match(Tokens.PROGRAM);
		match(Tokens.ID);
		match(Tokens.SEMI);
		declarations();
		subprogram_declarations();
		compound_statement();
		match(Tokens.PERIOD);
	}
	
	/**
	 * Parses identifier list
	 * Implements identifier_list -> id | id, identifier_list
	 */	
	public void identifier_list(){
		System.out.println("In identifier_list");
		match(Tokens.ID);
		if( currentToken == Tokens.COMMA){
			match(Tokens.COMMA);
			identifier_list();
		}
	}
	
	/**
	 * Parses declarations
	 * Implements declarations -> var identifier_list : type ; declarations | NULL
	 */
	public void declarations(){
		System.out.println("In declarations");
		if( currentToken == Tokens.VAR){
			match(Tokens.VAR);
			identifier_list();
			match(Tokens.COLON);
			type();
			match(Tokens.SEMI);
			declarations();
		}		
	}
	
	/**
	 * Parses type
	 * Implements type -> standard_type | array [ num : num ] of standard_type
	 */
	public void type(){
		System.out.println("In type");
		if (currentToken == Tokens.ARRAY){
			match(Tokens.ARRAY);
			match(Tokens.LEFT_BRACKET);
			match(Tokens.NUMBER);
			match(Tokens.COLON);
			match(Tokens.NUMBER);
			match(Tokens.RIGHT_BRACKET);
			match(Tokens.OF);
			standard_type();
		}
		else{
			standard_type();
		}
		
	}
	
	/**
	 * Parses standard type
	 * Implements standard_type -> integer | real (real not yet implemented into scanner
	 */
	public void standard_type(){
		System.out.println("In standard type");
		if(currentToken == Tokens.INTEGER){
			match(Tokens.INTEGER);		
		}
		if(currentToken == Tokens.REAL){
			match(Tokens.REAL);		
		}
	}
	
	/**
	 * Parses subprogram declarations
	 * Implements subprogram_declarations -> subprogram_declarations ; subprogram_declarations | NULL
	 */
	public void subprogram_declarations(){
		if(currentToken == Tokens.FUNCTION || currentToken == Tokens.PROCEDURE){
		subprogram_declaration();
		match(Tokens.SEMI);
		subprogram_declarations();
		}
	}
	
	/**
	 * Parses subprogram declaration
	 * Implements subprogram_declaration -> subprogram_head declarations subprogram_declarations compound_statement
	 */	
	public void subprogram_declaration(){
		subprogram_head();
		declarations();
		subprogram_declarations();
		compound_statement();
	}
	
	/**
	 * Parses Subprogram head
	 * Implements subprogram_head -> 	function id arguments : standard_type ; | procedure if arguments;
	 */
	public void subprogram_head(){
		System.out.println("In subprogram head");
		if( currentToken == Tokens.FUNCTION){
			match(Tokens.FUNCTION);
			match(Tokens.ID);
			arguments();
			match(Tokens.COLON);
			standard_type();
			match(Tokens.SEMI);
			
		}
		else if( currentToken == Tokens.PROCEDURE){
			match(Tokens.PROCEDURE);
			match(Tokens.ID);
			arguments();
			match(Tokens.SEMI);
		}
		else {
			System.out.println("Error in subprogram head");
		}
		
	}
	
	/**
	 * Parses arguments
	 * Implements arguments -> ( parameter_list ) | NULL
	 */
	public void arguments(){
		if(currentToken == Tokens.LEFT_PAREN){
			match(Tokens.LEFT_PAREN);
			parameter_list();
			match(Tokens.RIGHT_PAREN);
		}
		
	}
	
	/**
	 * Parses parameter_list
	 * Implements parameter_list -> identifier_list : type | identifier_list : type ; parameter_list
	 */
	public void parameter_list(){
		identifier_list();
		match(Tokens.COLON);
		type();
		if(currentToken == Tokens.SEMI){
			match(Tokens.SEMI);
			parameter_list();
		}
	}
	
	/**
	 * Parses Compound Statement
	 * Implements compound_statement -> begin optional_statements end
	 * 
	 */
	public void compound_statement(){
		match(Tokens.BEGIN);
		optional_statements();
		match(Tokens.END);
		
	}
	
	/**
	 * Parses Optional Statements
	 * Implements optional_statements -> statement_list | NULL
	 */
	public void optional_statements(){
		System.out.println("In Optional Statements");
		if(currentToken == Tokens.ID || currentToken == Tokens.BEGIN || currentToken == Tokens.IF
		|| currentToken == Tokens.WHILE || currentToken == Tokens.READ || currentToken == Tokens.WRITE){
		statement_list();
		}
		
	}
	
	/**
	 * Parses Statement List
	 * Implements statement_list -> statement | statement ; statement_list
	 */
	public void statement_list(){
		statement();
		if(currentToken == Tokens.SEMI){
			match(Tokens.SEMI);
			statement_list();
		}
		
	}
	
	/**
	 * Parses Statement
	 * Implements statement -> variable assignop expression | procedure_statement | compound_statement | if expression then statement else statement |
	 * while expression do statement | read (id) | write (expression)
	 */
	public void statement(){
		//Starting with a variable
		if(currentToken == Tokens.ID){
			match(Tokens.ID);
			if(currentToken == Tokens.LEFT_PAREN){
				procedure_statement();
			}
			else{
				variable();
				match(Tokens.ASSIGNOP);
				expression();
			}				
		} //end starting with variable
		
		//starts with begin
		else if(currentToken == Tokens.BEGIN){
			compound_statement();
		}
		
		//starts with if
		else if(currentToken == Tokens.IF){
			match(Tokens.IF);
			expression();
			match(Tokens.THEN);
			statement();
			match(Tokens.ELSE);
			statement();
		}
		
		//starts with if
		else if(currentToken == Tokens.IF){
			match(Tokens.IF);
			expression();
			match(Tokens.THEN);
			statement();
			match(Tokens.ELSE);
			statement();
		}
		
		//starts with while
		else if (currentToken == Tokens.WHILE){
			match(Tokens.WHILE);
			expression();
			match(Tokens.DO);
			statement();
		}
		
		//starts with read
		else if (currentToken == Tokens.READ){
			match(Tokens.READ);
			match(Tokens.LEFT_PAREN);
			match(Tokens.ID);
			match(Tokens.RIGHT_PAREN);
		}
		
		//starts with write
		else if (currentToken == Tokens.WRITE){
			match(Tokens.WRITE);
			match(Tokens.LEFT_PAREN);
			expression();
			match(Tokens.RIGHT_PAREN);
			
		}
		
	}
	
	/**
	 * Parses Expression
	 * Implements expression -> simple_expression | simple_expression relop simple_expression
	 */
	public void expression(){
		simple_expression();
		if(currentToken == Tokens.EQUAL || currentToken == Tokens.LESSTHAN || currentToken == Tokens.LESSTHANEQUAL ||
		   currentToken == Tokens.GREATERTHAN || currentToken == Tokens.GREATERTHANEQUAL){
		   match(currentToken);
		   simple_expression();
		}
	}
	
	/**
	 * Parses Simple Expression
	 * Implements simple_expression -> term simple_part | sign  term simple_part
	 */
	public void simple_expression(){
		if(currentToken == Tokens.ID || currentToken == Tokens.NUMBER || currentToken == Tokens.LEFT_PAREN || currentToken == Tokens.NOT){
			term();
			simple_part();
		}
		else if(currentToken == Tokens.PLUS || currentToken == Tokens.MINUS){
			sign();
			term();
			simple_part();
		}
	}
	
	/**
	 * Parses Term
	 * Implements term -> factor term_part
	 */
	public void term(){
		factor();
		term_part();
	}
	
	/**
	 * Parses Factor
	 * Implements factor -> id | id [ expression ] | id ( expression_list ) | num | ( expression ) | not factor
	 */
	public void factor(){
		if(currentToken == Tokens.ID){
			match(Tokens.ID);
			if(currentToken == Tokens.LEFT_BRACKET){
				match(Tokens.LEFT_BRACKET);
				expression();
				match(Tokens.RIGHT_BRACKET);
			}
			if(currentToken == Tokens.LEFT_PAREN){
				match(Tokens.LEFT_PAREN);
				expression_list();
				match(Tokens.RIGHT_PAREN);
			}
		}
		else if(currentToken == Tokens.NUMBER){
			match(Tokens.NUMBER);
		}
		else if(currentToken == Tokens.LEFT_PAREN){
			match(Tokens.LEFT_PAREN);
			expression();
			match(Tokens.RIGHT_PAREN);
		}
		else if(currentToken == Tokens.NOT){
			match(Tokens.NOT);
			factor();
		}
	}
	
	/**
	 * Parses Term Part
	 * Implements term_part -> mulop factor term_part | NULL
	 */
	public void term_part(){
		if(currentToken == Tokens.MULTIPLY || currentToken == Tokens.DIVIDE || currentToken == Tokens.DIV || currentToken == Tokens.MOD || currentToken == Tokens.AND){
			match(currentToken);
			factor();
			term_part();
		}
	}
	
	/**
	 * Parses Simple Part
	 * Implements simple_part -> addop term simple_part | NULL
	 */
	public void simple_part(){
		if(currentToken == Tokens.PLUS || currentToken == Tokens.MINUS || currentToken == Tokens.OR){
			match(currentToken);
			term();
			simple_part();
		}
		
	}
	
	/**
	 * Parses Sign
	 * Implements sign -> + | -
	 */
	public void sign(){
		match(currentToken);
		
	}
	

	/**
	 * Parses Procedure Statement - methods
	 * Implements procedure_statement -> id | id ( expression )
	 * ID matched in statement function 
	 */
	public void procedure_statement(){
		if(currentToken == Tokens.LEFT_PAREN){
			match(Tokens.LEFT_PAREN);
			expression_list();
			match(Tokens.RIGHT_PAREN);
		}
	}
	
	/**
	 * Parses Variables - arrays
	 * Implements variable -> id | id [ expression ]
	 * ID matched in statement function 
	 */
	public void variable(){
		if(currentToken == Tokens.LEFT_BRACKET){
			match(Tokens.LEFT_BRACKET);
			expression();
			match(Tokens.RIGHT_BRACKET);
		}
	}
	
	/**
	 * Parses Expression Lists
	 * Implements expression_list -> expression | expression , expression_list
	 */
	public void expression_list(){
		expression();
		if(currentToken == Tokens.COMMA){
			match(Tokens.COMMA);
			expression_list();
		}
		
	}
	
	/**
	 * Takes currentToken and matches with expected token according to grammer
	 * @param expectedToken
	 */
	public void match(Tokens expectedToken){
		System.out.println("Match " + expectedToken + " " + currentToken);
		if (currentToken == expectedToken){
			
			ntr = scanner.nextToken();
			
			currentToken = scanner.getToken();
			//System.out.println("Matching");
			
		}
		else {
			System.out.println("Error matching " + expectedToken + " found " + currentToken);
		}
		
	}
	

	
}
