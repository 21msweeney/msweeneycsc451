package scanner;

import java.io.File;
import java.util.Hashtable;



/**
 * @author Melissa Sweeney
 * Passes file to File Scanner to pick up tokens
 */


public class ScannerReadFile {

	/**
	 * @param Enter file to read in console
	 */
	public static void main(String[] args) {
		
	
		File input = new File("/Users/msweeney/Documents/CSC451/PascalCompiler/fooprogram.txt");

		Hashtable symbols = new SymbolTable();
		
		/*Pass the file to the FileScanner*/
        FileScanner fs = new FileScanner( input, symbols);
		
       
		NextTokenReturn heytoken;
		
		while ( (heytoken = fs.nextToken()) != NextTokenReturn.INPUT_COMPLETE){
			
			//If there is a token, print it out
			System.out.println("Return value is " + heytoken);
			if (heytoken == NextTokenReturn.TOKEN_AVAILABLE) {
				Tokens at = fs.getToken();
				Object attr = fs.getAttribute();
				System.out.println("Token: " + at + " Attribute [" + attr + "]");
			}
			
			else {
				System.out.println("Illegal stuff in the input file");
			}
		}
		
		
	}//end main
	
}//end scannerreadfile
