package scanner;

/**
* Return the token and move on to next
* @author Melissa Sweeney
*/

public enum NextTokenReturn {
    TOKEN_AVAILABLE, 
    TOKEN_NOT_AVAILABLE, 
    INPUT_COMPLETE,
}
