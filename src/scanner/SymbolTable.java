package scanner;

import java.util.Hashtable;


/**
* Add tokens to the SymbolTable
* @author Melissa Sweeney
*/


public class SymbolTable extends Hashtable {

	public SymbolTable () {
		super();
		this.put("+", Tokens.PLUS);
		this.put("*", Tokens.MULTIPLY);
		this.put("-", Tokens.MINUS);
		this.put("/", Tokens.DIVIDE);
		this.put("=", Tokens.EQUAL);
		this.put("<", Tokens.LESSTHAN);
		this.put("<=", Tokens.LESSTHANEQUAL);
		this.put(">", Tokens.GREATERTHAN);
		this.put(">=", Tokens.GREATERTHANEQUAL);
		this.put(":", Tokens.COLON);
		this.put(":=", Tokens.ASSIGNOP);
		this.put(";", Tokens.SEMI);
		this.put(",", Tokens.COMMA);
		this.put("[", Tokens.LEFT_BRACKET);
		this.put("]", Tokens.RIGHT_BRACKET);
		this.put("(", Tokens.LEFT_PAREN);
		this.put(")", Tokens.RIGHT_PAREN);
		this.put("_", Tokens.UNDERSCORE);
		this.put(".", Tokens.PERIOD);
		this.put("'", Tokens.APOSTROPHE);
		
		
		//reserved keywords
		this.put("begin", Tokens.BEGIN);
		this.put("end", Tokens.END);
		this.put("id", Tokens.ID);
		this.put("var", Tokens.VAR);
		this.put("array", Tokens.ARRAY);
		this.put("if", Tokens.IF);
		this.put("then", Tokens.THEN);
		this.put("else", Tokens.ELSE);
		this.put("while", Tokens.WHILE);
		this.put("do", Tokens.DO);
		this.put("not", Tokens.NOT);
		this.put("function", Tokens.FUNCTION);
		this.put("procedure", Tokens.PROCEDURE);
		this.put("div", Tokens.DIV);
		this.put("mod", Tokens.MOD);
		this.put("and", Tokens.AND);
		this.put("read", Tokens.READ);
		this.put("write", Tokens.WRITE);
		this.put("program", Tokens.PROGRAM);
		this.put("of", Tokens.OF);
		this.put("integer", Tokens.INTEGER);
		this.put("real", Tokens.REAL);
		this.put("then", Tokens.THEN);
		this.put("or", Tokens.OR);

	}
}


