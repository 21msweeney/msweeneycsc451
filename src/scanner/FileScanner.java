package scanner;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.util.Hashtable;


/**
* Scan the file from scannerreadfile
* run it thru the transition table
* @author Melissa Sweeney
*/

public class FileScanner {
	    
    private static final int START_STATE = 0;
    private static final int IN_NUMBER_STATE = 1;
    private static final int IN_IDENTIFIER_STATE = 2;
    private static final int IN_LESSTHANEQUAL = 3;
    private static final int IN_GREATERTHANEQUAL = 4;
    private static final int IN_ASSIGNOP_STATE = 5;
    private static final int ERROR_STATE = 50;
    private static final int SYMBOL_COMPLETE_STATE = 51;
    private static final int NUMBER_COMPLETE_STATE = 52;
    private static final int IDENTIFIER_COMPLETE_STATE = 53;
    private static final int LESSTHAN_COMPLETE_STATE = 54;    
    private static final int GREATERTHAN_COMPLETE_STATE = 55;
    private static final int ASSIGNOP_COMPLETE_STATE = 56;
    
    private int[][] transitionTable;  // table[state][char]
    private Tokens token;
    private Object attribute;
    private PushbackReader inputReader;
    private Hashtable symbolTable;


	/**
	 * Scan the file passed in from ScabberReadFile
	 * @param inputFile File to read
	 * @param symbolTable All recognized keywords and symbols
	 */
	    
    public FileScanner( File inputFile, Hashtable symbolTable) {
        
    	this.symbolTable = symbolTable;
        
    	try {
            this.inputReader = new PushbackReader(new FileReader(inputFile));
        }
        catch( Exception e) {
            e.printStackTrace();
            System.exit( 1);
        }
        
    	this.transitionTable = createTransitionTable();
    }
    
    /**
     * Returns lexeme
     * @return Attribute
     */
    public Object getAttribute() { return(this.attribute);}
    
    /**
     * Returns token availability
     * @return Token
     */
    public Tokens getToken() { return(this.token);}
     

   /**
    * Grabs the next token in the file
    * @return NextTokenReturn
    */
    public NextTokenReturn nextToken() {
        /*
         * Start in state 0
         */
    	int currentState = START_STATE;
        StringBuilder lexeme = new StringBuilder("");
       
        //While you are not in an error state
        while( currentState < ERROR_STATE ) {
            char currentChar = '\0';
            try {
                currentChar = (char) inputReader.read();
            }
            catch( Exception e) {
                // End of stream here.
                System.out.println("Should return input complete");
                return( NextTokenReturn.INPUT_COMPLETE);
            }
            
            //System.out.println("current char is actually " + (int)currentChar);
            if( currentChar == 65535) return( NextTokenReturn.INPUT_COMPLETE); // end of file
            if( currentChar > 127) return( NextTokenReturn.TOKEN_NOT_AVAILABLE);
            //System.out.print("State:" + currentState + " Char: " + currentChar + " (" + (int)currentChar + ")");
            
            int nextState = transitionTable[currentState][currentChar];
            System.out.println(" Next State: " + nextState + " current lexeme: ["+ lexeme.toString() + "]");
            
            switch( nextState) {
                // Start State
                case START_STATE:
                    lexeme = new StringBuilder("");
                    break;
                                    
                // find a number (an int at this point)
                case IN_NUMBER_STATE:
                    lexeme.append( currentChar);
                    break;
                case NUMBER_COMPLETE_STATE:
                    try {
                        inputReader.unread(currentChar);
                    }
                    catch( IOException ioe) {}
                    token = Tokens.NUMBER;
                    attribute = lexeme.toString();
                    break;
                
                // < present is it just that or followed by =
                case IN_LESSTHANEQUAL:
                	lexeme.append(currentChar);
                	break;
                              	
                case LESSTHAN_COMPLETE_STATE:
                	try{
                		inputReader.unread(currentChar);
                	}
                	catch( IOException ioe){}
                	 token = (Tokens)symbolTable.get(lexeme.toString());
                	 attribute = lexeme.toString();
                	 break;

                 // < present is it just that or followed by =
                 case IN_GREATERTHANEQUAL:
                 	lexeme.append(currentChar);
                  	break;
                                   	
                 case GREATERTHAN_COMPLETE_STATE:
                   	try{
                     	inputReader.unread(currentChar);
                     }
                   	catch( IOException ioe){}
                   	token = (Tokens)symbolTable.get(lexeme.toString());
                   	attribute = lexeme.toString();
                    break;
                    
                 //ASSIGNOP OR SEMICOLON HERE
                 case IN_ASSIGNOP_STATE:
                	 lexeme.append(currentChar);
                	 break;
                	 
                 case ASSIGNOP_COMPLETE_STATE:
                	 try{
                		 inputReader.unread(currentChar);
                	 }
                 catch (IOException ioe){}
                	 token = (Tokens)symbolTable.get(lexeme.toString());
                	 attribute = lexeme.toString();
                	 break;
                	 
                 //RESERVED WORDS HERE OR IDENTIFIERS
                 case IN_IDENTIFIER_STATE:
                    	lexeme.append( currentChar);
                    	break;
                 case IDENTIFIER_COMPLETE_STATE:
                    	try {
                    		inputReader.unread(currentChar);
                    	}
                        catch( IOException ioe) {}
                    	token = (Tokens)symbolTable.get(lexeme.toString());
                    	//System.out.println("ID Complete " + token + " " + lexeme);
                    	//If this is a reserved word return token
                    	if(token != null){
                    	attribute = lexeme.toString();
                    	}
                    	//This is not a reserved word so it returns a token
                    	else {
                    		token = Tokens.ID;
                    		attribute = lexeme.toString();
                    	}
                    	break;                    
                
                
                case SYMBOL_COMPLETE_STATE:
                	//You are only reading one symbol here
                	//if you are not coming from state 3 the Lessthan State
                	if(currentState != 3){
                		lexeme.append(currentChar);
                	}
                	token = (Tokens)symbolTable.get(lexeme.toString());
                	attribute = lexeme.toString();
                	break;
                	
                	
                case ERROR_STATE:
                	System.out.println("Scanner in error state current char" + currentChar);
                	token = null;
                	
                    return( NextTokenReturn.TOKEN_NOT_AVAILABLE);
            } // end switch
            currentState = nextState;
        } // end while currentState <= ERROR_STATE
        //System.out.println("Returning from next token " + token + "  " + attribute);
        return( NextTokenReturn.TOKEN_AVAILABLE);
    }
    

    
    
    
    
    
    
    
    
    private int[][] createTransitionTable() {
        return new int[][]
    {
        // State 0: Start
        {
           50,50,50,50,50,50,50,50,50, 0, 0,50,50, 0,50,50, //   0 -  15 (00-0f)
           50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50, //  16 -  31 (10-1f)
            0,50,50,50,50,50,50,51,51,51,51,51,51,51,51,50, //  32 -  47 (20-2f)
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5,51, 3,51, 4,50, //  48 -  63 (30-3f)
           50, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, //  64 -  79 (40-4f)
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,51,50,51,50,51, //  80 -  95 (50-5f)
           50, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, //  96 - 111 (60-6f)
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,50,50,50,50,50  // 112 - 127 (70-7f)
        },
        // State 1: In Number
        {
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //   0 -  15 (00-0f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  16 -  31 (10-1f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  32 -  47 (20-2f)
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,52,52,52,52,52,52, //  48 -  63 (30-3f)
           52,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50, //  64 -  79 (40-4f)
           50,50,50,50,50,50,50,50,50,50,50,52,52,52,52,52, //  80 -  95 (50-5f)
           52,52,50,50,50,50,50,50,50,50,50,50,50,50,50,50, //  96 - 111 (60-6f)
           50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50  // 112 - 127 (70-7f)
        },
        // State 2: In Identifier
        {
           50,50,50,50,50,50,50,50,50,50,53,50,50,50,50,50, //   0 -  15 (00-0f) new line 10
           50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50, //  16 -  31 (10-1f)
           53,50,50,50,50,50,50,50,50,53,53,53,53,53,53,53, //  32 -  47 (20-2f) space 32
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5,53,50,53,50,50, //  48 -  63 (30-3f) 0-9 48-57
           50, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, //  64 -  79 (40-4f) A-Z 65-90
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,50,50,50,50, 50, //  80 -  95 (50-5f) _ 95
            53, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, //  96 - 111 (60-6f) a-z 97-122
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,50,50,50,50,50,  // 112 - 127 (70-7f)
        },
        // State 3: In Less than Equal
        {
           50,50,50,50,50,54,50,50,50, 0,54,50,50, 0,50,50, //   0 -  15 (00-0f)
           50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50, //  16 -  31 (10-1f)
           54,50,50,50,50,50,50,50,50,50,51,51,50,51,50,51, //  32 -  47 (20-2f)
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,50,50,50, 3,50,50, //  48 -  63 (30-3f)
           50,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54, //  64 -  79 (40-4f)
           54,54,54,54,54,54,54,54,54,54,54,50,50,50,50,50, //  80 -  95 (50-5f)
           50,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54, //  96 - 111 (60-6f)
           54,54,54,54,54,54,54,54,54,54,54,50,50,50,50,50  // 112 - 127 (70-7f)
        },
        // State 4: In Greater than Equal
        {
           50,50,50,50,50,54,50,50,50, 0,55,50,50, 0,50,50, //   0 -  15 (00-0f)
           50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50, //  16 -  31 (10-1f)
           55,50,50,50,50,50,50,50,50,50,51,51,50,51,50,51, //  32 -  47 (20-2f)
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,50,50,50, 4,50,50, //  48 -  63 (30-3f)
           50,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55, //  64 -  79 (40-4f)
           55,55,55,55,55,55,55,55,55,55,55,50,50,50,50,50, //  80 -  95 (50-5f)
           50,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55, //  96 - 111 (60-6f)
           55,55,55,55,55,55,55,55,55,55,55,50,50,50,50,50  // 112 - 127 (70-7f)
        },
        // State 5: In Assignop
        {
           50,50,50,50,50,50,50,50,50,56,55,50,50, 0,50,50, //   0 -  15 (00-0f)
           50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50, //  16 -  31 (10-1f)
           56,50,50,50,50,50,50,50,50,50,51,51,50,51,50,51, //  32 -  47 (20-2f)
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,50,50,50, 5,50,50, //  48 -  63 (30-3f)
           50,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55, //  64 -  79 (40-4f)
           55,55,55,55,55,55,55,55,55,55,55,50,50,50,50,50, //  80 -  95 (50-5f)
           50,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55, //  96 - 111 (60-6f)
           55,55,55,55,55,55,55,55,55,55,55,50,50,50,50,50  // 112 - 127 (70-7f)
        },    
        };
        
        
    }

}//end FileScanner
